import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class PrimaryNumber extends RecursiveTask<List> {

    private static final int SEQUENTIAL_THRESHOLD = 50;

    private int start;
    private int end;
    private volatile List<Integer> list = Collections.synchronizedList(new ArrayList<>());
    private final ForkJoinPool fjPool = new ForkJoinPool();
    private Long time = 0L;

    PrimaryNumber(int start, int end){
        this.start = start;
        this.end = end;
    }


    private boolean isPrimaryNumber(int number){

        if(number == 1){
            return false;
        }
        if(number == 2 || number == 3){
            return true;
        }
        if(number > 3){

            if(number % 2 == 0){
                return false;
            }

            for(int i=3; i<=Math.sqrt(number); i=i+2){
                if(number % i == 0){
                    return  false;
                }
            }
        }
        return true;
    }

    public Long getTime(){
        return time;
    }


    @Override
    protected List compute() {
        if(end - start <= SEQUENTIAL_THRESHOLD) {
            long primaryNumbersCount = 0;
            for(int i=start; i<end; i++){
                if(isPrimaryNumber(i)){
                    primaryNumbersCount+=1;
                    list.add(i);
                }
            }

            return list;
        } else {
            int mid = start + (end - start) / 2;
            PrimaryNumber left  = new PrimaryNumber(start, mid);
            PrimaryNumber right = new PrimaryNumber(mid, end);
            long startTime = System.nanoTime();
            left.fork();
            List listR = right.compute();
            List listL = left.join();
            List returnList = new ArrayList();

            returnList.addAll(listL);
            returnList.addAll(listR);
            long endTime = System.nanoTime() - startTime;
            time += endTime;

            return returnList;
        }
    }

    List countPrimes(int start, int end){
        System.out.println(getTime());
        return fjPool.invoke(new PrimaryNumber(start,end));
    }

}
